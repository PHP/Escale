<?php

define('K', pi()/180.0);

/**
 * Pour récupérer l'heure UT des changements de saison à la minute prêt (±1minute). 
 * Source : <a href="http://www.jgiesen.de/astro/astroJS/seasons/">Equinoxes and Solstices</a>
 * 
 * @author zefling (portage)
 * @version 1.0
 * @link http://ikilote.net/fr/Blog/Techno-magis/110.html
 */
class Seasons {
	
	public $y;
	public $_Year;
	public $yearArray   = array();
	public $springArray = array();
	public $summerArray = array();
	public $autumnArray = array();
	public $winterArray = array();
	public $nSpring;
	public $n19;
	public $n20;
	public $n21;
	public $n22;

	public function computeSeasonsInter ($year1, $year2) {
		
		if ($year2 < $year1) {
			return null;
		}
		
		$number = $year2 - $year1;

		$this->nSpring = 0;
		$this->n19     = 0;
		$this->n20     = 0;
		$this->n21     = 0;
		$this->n22     = 0;
		
		for ($i=0 ; $i<= $number ; $i++) {
			$this->y     = $year1 + $i;
			$this->_Year = $this->y;
			$this->yearArray[$i] = $this->y;
			$this->y = ($this->y - 2000)/1000;
			
			$this->springArray[$i] = $this->spring();
			$this->summerArray[$i] = $this->summer();
			$this->autumnArray[$i] = $this->autumn();
			$this->winterArray[$i] = $this->winter();
		}
		
		return array(
			"spring" => $this->springArray,
			"summer" => $this->summerArray,
			"autumn" => $this->autumnArray,
			"winter" => $this->winterArray
		);
		
	}
	
	public function computeSeasons($year) {

		$this->y     = ($year - 2000.0) / 1000.0;
		$this->_Year = $year;
		
		return array(
			"spring" => $this->spring(),
			"summer" => $this->summer(),
			"autumn" => $this->autumn(),
			"winter" => $this->winter()
		);
	}

	private function S($t) {
				
		return 
		      485*cos(K*(324.96 +   1934.136*$t))
			+ 203*cos(K*(337.23 +  32964.467*$t))
			+ 199*cos(K*(342.08 +     20.186*$t))
			+ 182*cos(K*( 27.85 + 445267.112*$t))
			+ 156*cos(K*( 73.14 +  45036.886*$t))
			+ 136*cos(K*(171.52 +  22518.443*$t))
			+  77*cos(K*(222.54 +  65928.934*$t))
			+  74*cos(K*(296.72 +   3034.906*$t))
			+  70*cos(K*(243.58 +   9037.513*$t))
			+  58*cos(K*(119.81 +  33718.147*$t))
			+  52*cos(K*(297.17 +    150.678*$t))
			+  50*cos(K*( 21.02 +   2281.226*$t))
	
			+  45*cos(K*(247.54 +  29929.562*$t))
			+  44*cos(K*(325.15 +  31555.956*$t))
			+  29*cos(K*( 60.93 +   4443.417*$t))
			+  18*cos(K*(155.12 +  67555.328*$t))
	
			+  17*cos(K*(288.79 +   4562.452*$t))
			+  16*cos(K*(198.04 +  62894.029*$t))
			+  14*cos(K*(199.76 +  31436.921*$t))
			+  12*cos(K*( 95.39 +  14577.848*$t))
			+  12*cos(K*(287.11 +  31931.756*$t))
			+  12*cos(K*(320.81 +  34777.259*$t))
			+   9*cos(K*(227.73 +   1222.114*$t))
			+   8*cos(K*( 15.45 +  16859.074*$t));
		
	}

	private function march() {
		//var JDE0 = 2451623.80984 + (365242.37404 + (0.05169  - (0.00411 - 0.00057*y)*y)*y)*y;
		$JDE0 = 2451623.80984 
			+ 365242.37404*$this->y 
			+ 0.05169*$this->y*$this->y 
			- 0.00411*$this->y*$this->y*$this->y 
			- 0.00057*$this->y*$this->y*$this->y*$this->y;
		
		$T  = ($JDE0 - 2451545.0)/36525.0;
		$W  = 35999.373*$T - 2.47;
		$W  = K*$W;
		$dL = 1.0 + 0.0334*cos($W) + 0.0007*cos(2*$W);

		$marchT   = $JDE0 + (0.00001*$this->S($T))/$dL - (66 + ($this->_Year-2000)*1)/86400;
		
		return $marchT;
	}

	private function june() {
		//var JDE0 = 2451716.56767 + (365241.62603 + (0.00325  - (0.00888 - 0.00030*y)*y)*y)*y;
		$JDE0 = 2451716.56767 
			+ 365241.62603*$this->y 
			+ 0.00325*$this->y*$this->y  
			+ 0.00888*$this->y*$this->y*$this->y 
			- 0.00030*$this->y*$this->y*$this->y*$this->y;

		$T  = ($JDE0 - 2451545.0)/36525.0;
		$W  = 35999.373*$T - 2.47;
		$W  = K*$W;
		$dL = 1.0 + 0.0334*cos($W) + 0.0007*cos(2*$W);
		
		$juneT = $JDE0 + (0.00001*$this->S($T))/$dL - (66.0 + ($this->_Year-2000)*1.0)/86400.0;
		return $juneT;
	}

	private function september() {
		//var JDE0 = 2451810.21715 + (365242.01767 + (0.11575  - (0.00337 - 0.00078*y)*y)*y)*y;
		$JDE0 = 2451810.21715 
			+ 365242.01767*$this->y 
			- 0.11575*$this->y*$this->y  
			+ 0.00337*$this->y*$this->y*$this->y 
			+ 0.00078*$this->y*$this->y*$this->y*$this->y;

		$T  = ($JDE0 - 2451545.0)/36525.0;
		$W  = 35999.373*$T - 2.47;
		$W  = K*$W;
		$dL = 1.0 + 0.0334*cos($W) + 0.0007*cos(2*$W);
		
		$septemberT = $JDE0 + (0.00001*$this->S($T))/$dL - (66.0 + ($this->_Year-2000)*1.0)/86400.0;
		return $septemberT;
	}

	private function december() {
		//var JDE0 = 2451900.05952 + (365242.74049 + (0.06223  - (0.00823 - 0.00032*y)*y)*y)*y;
		$JDE0 = 2451900.05952 + 365242.74049*$this->y - 0.06223*$this->y*$this->y 
			- 0.00823*$this->y*$this->y*$this->y + 0.00032*$this->y*$this->y*$this->y*$this->y;

		$T  = ($JDE0 - 2451545.0)/36525.0;
		$W  = 35999.373*$T - 2.47;
		$W  = K*$W;
		$dL = 1.0 + 0.0334*cos($W) + 0.0007*cos(2*$W);
		
		$decemberT = $JDE0 + (0.00001*$this->S($T))/$dL - (66.0 + ($this->_Year-2000)*1.0)/86400.0;
		return $decemberT;
	}

	private function caldat ($JD) {
			
		$JD0 = floor($JD + 0.5);
		$B   = floor(($JD0-1867216.25)/36524.25);
		$C   = $JD0 + $B - floor($B/4) + 1525.0;
		$D   = floor(($C-122.1)/365.25);
		$E   = 365.0*$D + floor($D/4);
		$F   = floor(($C-$E)/30.6001);
		$day = floor($C-$E+0.5) - floor(30.6001*$F);

		$month = $F - 1 - 12*floor($F/14);
		$year  = $D - 4715 - floor((7+$month)/10);
		$hour  = 24.0*($JD + 0.5 - $JD0);
		$diff  = abs($hour) - floor(abs($hour));
		$min   = floor(round($diff*60.0));
		if ($min == 60) {
			$min  = 0;
			$hour = $hour + 1.0;
		}
				
		if ($day == 19) $this->n19++;
		if ($day == 20) $this->n20++;
		if ($day == 21) $this->n21++;
		if ($day == 22) $this->n22++;
		
		return sprintf('%04d-%02d-%02d %02d:%02d', $year, $month, $day, $hour, $min);
		
	}

	private function spring() {
		$this->nSpring++;
		return $this->caldat($this->march());
	}

	private function summer() {
		return $this->caldat($this->june());
	}

	private function autumn() {
		return $this->caldat($this->september());
	}

	private function winter() {
		return $this->caldat($this->december());
	}

}