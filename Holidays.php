<?php
/*

Copyright © Zéfling - 2014 ( http://ikilote.net/fr/Blog/Techno-magis.html )

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

The Software is provided "as is", without warranty of any kind, express or
implied, including but not limited to the warranties of merchantability,
fitness for a particular purpose and noninfringement. In no event shall the
authors or copyright holders be liable for any claim, damages or other liability,
whether in an action of contract, tort or otherwise, arising from, out of or
in connection with the software or the use or other dealings in the Software.

*/

include_once "Seasons.php";

/**
 * Les jours fériés
 * @author zefling
 * @version 0.1 (2014-06-14)
 * @license Expat
 *
 */
class Holidays {
	
	const VERSION_CODE     = '0.1';
	const VERSION_CODE_NUM = '0.1 (2014-06-14)';
	
	/**
	 * Cette fonction retourne un tableau de timestamp correspondant
	 * aux jours fériés en France pour une année donnée.
	 * @param $year int L'année (si null prend d'année courante. Défaut : null)
	 * @param $reunion bool spécifique à la Réunion (Défaut : false)
	 * @return array:int la liste timestamp des jours fériés
	 */
	function getHolidays_France($year = null, $reunion = false) {
		if ($year === null) {
			$year = intval(date('Y'));
		}
	
		// Dimanche de Pâques
		$easterDate  = easter_date($year);
		$easterDay   = date('j', $easterDate);
		$easterMonth = date('n', $easterDate);
		$holidays = array(
			// Dates fixes
			mktime(0, 0, 0, 1,  1,  $year),  // 1er janvier
			mktime(0, 0, 0, 7,  14, $year),  // Fête nationale
			mktime(0, 0, 0, 8,  15, $year),  // Assomption
			mktime(0, 0, 0, 11, 1,  $year),  // Toussaint
			mktime(0, 0, 0, 11, 11, $year),  // Armistice
			mktime(0, 0, 0, 12, 25, $year),  // Noël

			// Dates variables
			mktime(0, 0, 0, $easterMonth, $easterDay + 1,  $year), // lundi de Pâques
			mktime(0, 0, 0, $easterMonth, $easterDay + 39, $year), // jeudi de l'Ascension
			mktime(0, 0, 0, $easterMonth, $easterDay + 50, $year), // lundi de Pentecôte
		);
		
		if ($year >= 1948) {
			$holidays[] = mktime(0, 0, 0, 5,  1,  $year);  // Fête du travail
		}

		if ($reunion && $year >= 1981) {
			$holidays[] = mktime(0, 0, 0, 12, 20,  $year);  // Abolition de l’esclavage
		}
		
		sort($holidays);
	
		return $holidays;
	}
	
	/**
	 * Cette fonction retourne un tableau de timestamp correspondant
	 * aux jours fériés au Japon pour une année donnée.
	 * @param $year int L'année (si null prend d'année courante. Défaut : null)
	 * @return array:int la liste timestamp des jours fériés
	 */
	public static function getHolidays_Japan($year = null) {
		if ($year === null) {
			$year = intval(date('Y'));
		}
		
		$seasons = new Seasons();
		
		$s = $seasons->computeSeasons($year);
		
		$spring = date_parse($s['spring']);
		$autumn = date_parse($s['autumn']);
		
		// golden week
		$kinenbi   = self::getDay( 3,  5, $year);
		$kinenbi_f = count($kinenbi) == 1;
		if ($year >= 2007) {
			$midori   = self::getDay( 4 + ($kinenbi_f ? 0 : 1),  5, $year);
			$midori_f = count($midori) == 1;
		} else {
			$midori   = array();
			$midori_f = true;
		}
		$kodomo    = self::getDay( 5 + ($kinenbi_f && $midori_f ? 0 : 1),  5, $year);
		
		// merge
		$holidays = array_merge(
			// Ganjitsu (元日)
			self::getDay(  1,  1, $year), 
			
			// Seijin no Hi (成人の日) : ~1999 (1月15日) / 2000~ (1月の第2月曜日)
			$year <  2000 ? self::getDay( 15,  1, $year) : self::getDayCalc( 1, 2, 1, $year),
			
			// Kenkoku Kinenbi (建国記念の日)
			$year >= 1967 ? self::getDay( 11,  2, $year) : array(), 
				
			// golden week
			self::getDay( 29,  4, $year),   // Shōwa no Hi (昭和の日)
			$kinenbi,                 // Kempō Kinenbi (憲法記念日)
			$midori,                  // Midori no Hi (みどりの日) : ~2007 (5月4日)
			$kodomo,                  // Kodomo no Hi (こどもの日)
			
			// Bunka no Hi (文化の日)
			self::getDay(  3, 11, $year),  
			
			// Kinrō Kansha no Hi (勤労感謝の日)
			self::getDay( 23, 11, $year) ,
			
			// Tennō Tanjōbi (天皇誕生日)
			$year >= 1989 ? self::getDay( 23, 12, $year) : array(),  
			
			// Équinoxes
			self::getDay($spring['day'], $spring['month'], $year, $spring['hour'] + 9), // Shunbun no Hi (春分の日)
			self::getDay($autumn['day'], $autumn['month'], $year, $autumn['hour'] + 9)  // Shūbun no Hi (秋分の日)
			
		);
		
		
		if ($year >= 1966) {
			$holidays = array_merge($holidays,
				// Keirō no Hi (敬老の日) : 1966-2002 (9月15日) / 2003~ (9月の第3月曜日)
				$year < 2000 ? self::getDay( 15,  9, $year) : self::getDayCalc( 9, 3, 1, $year),
				// Taiiku no Hi (体育の日) : 1966-2002 (10月10日) / 2003~ (10月の第2月曜日)
				$year < 2000 ? self::getDay( 10, 10, $year) : self::getDayCalc(10, 2, 1, $year)
			);
		}
		
		// Umi no Hi (海の日) : 1996-2002 (7月20日) / 2003~ (7月の第3月曜日)
		if ($year >= 1996) {
			$holidays = array_merge($holidays, 
				$year < 2003 ? self::getDay( 20,  7, $year) : self::getDayCalc( 7, 3, 1, $year)
			);  
		}
		
		// nouveau jour férié à partir de 2016
		if ($year >= 2016) {
			$holidays[] = self::getDay( 11, 8, $year); // Yama no hi (山の日)
		}
		
		sort($holidays);
	 
		return $holidays;
	}
	
	/**
	 * Donne un timestamp du jour nième de semaine d'un mois. (ex. 3e lundi de mars)
	 * @param int $mount mois
	 * @param int $time nième [jour de la semaine] du mois
	 * @param int $weekday jour de la semaine (0 : dimanc(($year < 2000) ? self::getDay(  1,  15, $year) : ndi, etc.)
	 * @param int $year année
	 * @return number timestamp
	 */
	public static function getDayCalc ($mount, $time, $weekday, $year) {
		
		$date = mktime(0, 0, 0, $mount, 1,  $year);
		$jour = date('w' ,$date);
		
		if ($jour == $weekday) {
			$i = 1;
		} elseif ($jour < $weekday ) {
			$i = $weekday - $jour + 1;
		} else {
			$i = 8 - $jour + $weekday;
		}
		
		return self::getDayRecup(mktime(0, 0, 0, $mount, $i + ($time -1) * 7 ,  $year));
	}
	
	/**
	 * Donne un timestamp du jour courant sauf si c'est un dimanche, proposer le lundi suivant
	 * @param int $day jour
	 * @param int $mount mois
	 * @param int $year année
	 * @return number timestamp
	 */
	public static function getDay($day, $mount, $year, $hour = 0) {
		return self::getDayRecup(mktime($hour, 0, 0, $mount, $day,  $year));
	}
	
	/**
	 * Si c'est un dimanche, retourner le dimanche plus le lundi, sinon le jour seul
	 * @param int $date timestamp
	 * @return array timestamp du ou des jours fériés
	 */
	public static function getDayRecup ($date) {
		$dates = array();
		$dates[] = $date;
		if (date('w' ,$date) == 0) {
			$dates[] = strtotime("+1 day", $date);
		}
		return $dates;
	}
	
}